import React from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import * as Fa6Icons from "react-icons/fa6";

export const SidebarData = [
  {
    title: "Home",
    path: "/",
    icon: <AiIcons.AiFillHome />,
    cName: "nav-text",
  },
  {
    title: "Zimmer",
    path: "/bilder",
    icon: <FaIcons.FaBed />,
    cName: "nav-text",
  },
  {
    title: "Unterkunft",
    path: "/unterkunft ",
    icon: <FaIcons.FaHome />,
    cName: "nav-text",
  },
  {
    title: "Standort",
    path: "/standort",
    icon: <Fa6Icons.FaMapLocationDot />,
    cName: "nav-text",
  },
  {
    title: "Information",
    path: "/information",
    icon: <FaIcons.FaInfoCircle  />,
    cName: "nav-text",
  },
];