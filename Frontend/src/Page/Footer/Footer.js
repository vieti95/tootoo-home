import React from 'react';
import './Footer.css';

function Footer() {
  return (
    <footer className="footer">
  <div className="left-content">
    <p className='Footer_Information'>© 2024 TooToo, Inc.</p>
    </div>
  <div className="center-content">
    <a href="/impressum">Impressum</a>
    <a href="/datenschutz">Datenschutz</a>
  </div>
  
  <div className="right-content"></div>
</footer>


  );
}

export default Footer;
