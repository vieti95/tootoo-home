import React from "react";
import "./FinalUnterkunft.css";
import Unterkunft from "./Unterkunft/Unterkunft";
import Unterkunft2 from "./Unterkunft2/Unterkunft2";

const FinalUnterkunft = () => {
  return (
   <>
   <div>
    <h1 className='Titel-Unterkunkft'>Über diese Unterkunft</h1>
    </div>
    <div className="row">
     <Unterkunft2/>
     <hr className="Wand"></hr>
     <Unterkunft className="Unterkunft"/>
    </div>
   </>
  );
};

export default FinalUnterkunft;
