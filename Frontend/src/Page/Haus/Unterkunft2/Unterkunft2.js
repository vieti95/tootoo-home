import React from "react";
import "./Unterkunft2.css";
import BedroomChildIcon from '@mui/icons-material/BedroomChild';
import { FaPeopleRoof } from "react-icons/fa6";
import { FaShower } from "react-icons/fa";
import { CiParking1 } from "react-icons/ci";


const Unterkunft2 = () => {
  return (
    <>
    <div className="Unterkunft2">
    <div className="Zimmermodul">
    <BedroomChildIcon className="Zimmer"></BedroomChildIcon>
    <p className="Zimmer_Text">Zimmer in Reihenhaus
    <p className="Zimmer_Text2">Dein eigenes Zimmer und gemeinsam genutzte Räume.</p>
    <p className="Zimmer_Text3">Sie werden höchstwahrscheinlich das Haus mit anderen Gästen teilen (Arbeiter, Messeteilnehmer etc.).</p>
    </p>
    </div>

    <div className="Gemeinsammodul">
    <FaPeopleRoof className="Gemeinsam"></FaPeopleRoof>
    <p className="Gemeinsam_Text">Gemeinsam genutzte Räume
    <p className="Gemeinsam_Text2">Du nutzt Teile der Unterkunft gemeinsam mit anderen Gästen.</p>
    </p>
    </div>

    <div className="Showermodul">
    <FaShower className="Shower"></FaShower>
    <p className="Shower_Text">Gemeinsam genutztes Bad
    <p className="Shower_Text2">Du nutzt das Badezimmer gemeinsam mit anderen.</p>
    </p>
    </div>

    <div className="Parkmodul">
    <CiParking1 className="Park"></CiParking1>
    <p className="Park_Text">Nutze den Komfort kostenloser Parkmöglichkeiten
    <p className="Park_Text2">Dies ist eine der wenigen Unterkünfte in der Gegend mit kostenlosen Parkplätzen.</p>
    </p>
    </div>
    </div>
    </>
  );
};

export default Unterkunft2;
