import './Unterkunft.css';
import React from 'react';
import { LuBedSingle } from "react-icons/lu";
import { GiShower } from "react-icons/gi";
import Collapsible from './Collapsible.js';
import { FaToilet } from "react-icons/fa";
import { FaHotTub } from "react-icons/fa";
import Icon from '@mdi/react';
import { mdiHairDryer } from '@mdi/js';
import { BiSolidBlanket } from "react-icons/bi";
import { GiHanger } from "react-icons/gi";
import { TbHanger } from "react-icons/tb";
import { CgSmartHomeWashMachine } from "react-icons/cg";
import { PiTelevisionSimpleBold } from "react-icons/pi";
import { BiSolidDryer } from "react-icons/bi";
import { mdiHvac } from '@mdi/js';
import { mdiSmokeDetectorVariant } from '@mdi/js';
import MedicalServicesIcon from '@mui/icons-material/MedicalServices';
import { FaWifi } from "react-icons/fa";
import { mdiCountertop } from '@mdi/js';
import { FaKitchenSet } from "react-icons/fa6";
import FlatwareIcon from '@mui/icons-material/Flatware';
import { MdKitchen } from "react-icons/md";
import { mdiFridgeIndustrial } from '@mdi/js';
import { LuMicrowave } from "react-icons/lu";
import { mdiStove } from '@mdi/js';
import { IoCafe } from "react-icons/io5";
import { FaWineGlass } from "react-icons/fa";
import { mdiTableChair } from '@mdi/js';
import { FaDoorClosed } from "react-icons/fa6";
import { MdBalcony } from "react-icons/md";
import { IoMdPeople } from "react-icons/io";
import { FaCar } from "react-icons/fa";
import { SlCalender } from "react-icons/sl";
import { GoPasskeyFill } from "react-icons/go";

export default function Unterkunft()
{

    return(
        <div className='Unterkunft'>
         <Collapsible summary="Wo du schlafen wirst">
         <p className='Zimmerntext'>Ein Reihenhaus mit 5 grossräumigen Mehrbett-Zimmern: 12 Einzelbetten (90x200)</p>
        <div className='Design'>
            <p className='Schlafzimmer1'>
            <LuBedSingle className='bed1'></LuBedSingle> <LuBedSingle className='bed2'></LuBedSingle>
            <p className='text'>Schlafzimmer 1</p>
            <p className='text1'>2 Einzelbetten </p> 
            </p>

            <p className='Schlafzimmer2'>
            <LuBedSingle className='bed1'></LuBedSingle> <LuBedSingle className='bed2'></LuBedSingle> <LuBedSingle className='bed3'></LuBedSingle>
            <p className='text'>Schlafzimmer 2 </p>
            <p className='text1'>3 Einzelbetten </p>
            </p>

            <p className='Schlafzimmer3'>
            <LuBedSingle className='bed1'></LuBedSingle> <LuBedSingle className='bed2'></LuBedSingle> 
            <p className='text'>Schlafzimmer 3</p>
            <p className='text1'>2 Einzelbetten</p>
            </p>
            </div>

            <div className='Design1'>
            <p className='Schlafzimmer4'>
            <LuBedSingle className='bed1'></LuBedSingle> <LuBedSingle className='bed2'></LuBedSingle> 
            <p className='text'>Schlafzimmer 4</p>
            <p className='text1'>2 Einzelbetten</p>
            </p>

            <p className='Schlafzimmer5'>
            <LuBedSingle className='bed1'></LuBedSingle> <LuBedSingle className='bed2'></LuBedSingle> <LuBedSingle className='bed3'></LuBedSingle> 
            <p className='text'>Schlafzimmer 5</p>
            <p className='text1'>3 Einzelbetten </p>
            </p>
        </div>
      </Collapsible>
      <Collapsible summary="Das bietet dir dieses Unterkunft">
      <div className='Anbieten'>
      <div className='Badezimmermodul'> 
      <h2 className='Badezimmer_Titel'>Badezimmer</h2>
                <GiShower className='Dusche'></GiShower>
                 <p className='Dusche_Text'>2 Bäder mit Dusche</p>
                 <FaToilet className='Toilette'></FaToilet>
                 <p className='Toilette_Text'>2 Toiletten, 1 Pissoir</p>
                 <FaHotTub className='Warmwasser'></FaHotTub>
                 <p className='Warmwasser_Text'>Warmwasser</p>
                 <Icon className="Foehn" path={mdiHairDryer} size={2.5} />
                 <p className='Foehn_Text'>Föhn</p>
        </div>
        <div className='Schlafzimmermodul'>
            <h2 className='Schlafzimmer_Titel'>Schlafzimmer und Wäsche</h2>
            < BiSolidBlanket className='Bettwaesche'></BiSolidBlanket>
            <p className='Bettwaesche_Text'>Bettwäsche</p>
            <p className='Bettwaesche_Text2'>Bettwäsche aus Baumwolle</p>
            <GiHanger className='Kleiderbuegel'></GiHanger>
            <p className='Kleiderbuegel_Text'>Kleiderbügel</p>
            <TbHanger className='Waschestaender'></TbHanger>
            <p className='Waschestaender_Text'>Wäscheständer für Kleidung</p>
            <CgSmartHomeWashMachine className='Waschmaschine'></CgSmartHomeWashMachine>
            <p className='Waschmaschine_Text'>1 Münzwaschmaschine im Keller (nur 1€ Münzen)</p>
            <BiSolidDryer className='Trockner'></BiSolidDryer>
            <p className='Trockner_Text'>Trockner</p>
        </div>
        <div className='Unterhaltungmodul'>
        <h2 className='Unterhaltung_Titel'>Unterhaltung</h2>
        <PiTelevisionSimpleBold className='Fernseher'></PiTelevisionSimpleBold>
            <p className='Fernseher_Text'>1 Wohnzimmer mit Smart TV</p>
        </div>
        <div className='Heizungmodul'>
        <h2 className='Heizung_Titel'>Heizung und Klimaanlage</h2>
        <Icon className="Heizung" path={mdiHvac} size={2.5} />
            <p className='Heizung_Text'>Heizung</p>
        {/*<AcUnitIcon className="Klima"></AcUnitIcon>
        <p className='Klima_Text'>Klimaanlage</p>*/}
        </div>
        <div className='Zuhausemodul'>
         <h2 className='Zuhause_Titel'>Sicheres Zuhause</h2>  
         <Icon className='Rauchmelder' path={mdiSmokeDetectorVariant} size={2.5} />
         <p className='Rauchmelder_Text'>Rauchmelder</p>
        <MedicalServicesIcon className="ErsteHilfe"></MedicalServicesIcon>
        <p className='ErsteHilfe_Text'>Erste Hilfe Set</p>
        </div>
        <div className='Wlanmodul'>
        <h2 className='Wlan_Titel'>Internet und Büro</h2>
        <FaWifi className='Wlan'></FaWifi>
        <p className='Wlan_Text'>WLAN</p>
        <div className='Kuechemodul'>
        <h2 className='Kueche_Titel'>Küche und Essenzimmer</h2>
        <Icon className='kitchen' path={mdiCountertop} size={2.5} />
        <p className='kitchen_Text'>1 helles, grosses voll ausgestattete Küche</p>
        <p className='kitchen_Text2'>Ort, an dem Gäste ihre eigenen Mahlzeiten zubereiten können</p>
        <FaKitchenSet className='kochen'></FaKitchenSet>
        <p className='kochen_Text'>Grundausstattung zum Kochen</p>
        <p className='kochen_Text2'>Töpfe und Pfannen, Öl, Salz und Pfeffer</p>
        <FlatwareIcon className="besteck"></FlatwareIcon>
        <p className='besteck_Text'>Geschirr und Besteck</p>
        <p className='besteck_Text2'>Schüsseln, Essstäbchen, Teller, Tassen usw.</p>
        <MdKitchen className='Kuehlschrank'></MdKitchen>
        <p className='Kuehlschrank_Text'>Kühlschrank</p>
        <Icon className="Gefrier" path={mdiFridgeIndustrial} size={2.5} />
        <p className='Gefrier_Text'>Gefrierschrank</p>
        <LuMicrowave className='Mikrowelle'></LuMicrowave>
        <p className='Mikrowelle_Text'>Mikrowelle</p>
        <Icon className="Ofen" path={mdiStove} size={2.5} />
        <p className='Ofen_Text'>Ofen von Simfer</p>
        <IoCafe className='Cafe'></IoCafe>
        <p className='Cafe_Text'>Kaffeemaschine</p>
        <FaWineGlass className='Wein'></FaWineGlass>
        <p className='Wein_Text'>Weingläser</p>
        <Icon className='esstisch' path={mdiTableChair} size={2.5} />
        <p className='esstisch_Text'>Esstisch</p>
        </div>
        <div className='Rundmodul'>
        <h2 className='Rund_Titel'>Rund um die Unterkunft</h2>
        <FaDoorClosed className='Door'></FaDoorClosed>
        <p className='Door_Text'>Privater Eingang</p>
        <p className='Door_Text2'>Eigener Straßenzugang oder Hauseingang</p>
        </div>
        <div className='Aussenmodul'>
        <h2 className='Aussen_Titel'>Außenbereiche</h2>
        <MdBalcony className='Aussen'></MdBalcony>
        <p className='Aussen_Text'>Gemeinsam genutzt Innenhof oder Balkon</p>
        <p className='Aussen_Text2'>Raucherecke draussen mit Sitzbank</p>
        <IoMdPeople className='Nachbar'></IoMdPeople>
        <p className='Nachbar_Text'>ruhige Gegend und Nachbarschaft</p>
        </div>
        <div className='Parkenmodul'>
        <h2 className='Parken_Titel'>Parken und zusätzlicher Zugang</h2>
        <FaCar className='Parken'></FaCar>
        <p className='Parken_Text'>- Parkplatz inkl. (first come first serve)</p>
        </div>
        <div className='Servicemodul'>
        <h2 className='Service_Titel'>Services</h2>
        <SlCalender className='Kalender'></SlCalender>
        <p className='Kalender_Text'>Langzeitaufenthalte sind möglich</p>
        <p className='Kalender_Text2'>Gäste können 28 Tage oder länger bleiben</p>
        <GoPasskeyFill className='Key'></GoPasskeyFill>
        <p className='Key_Text'>Begrüßung durch Gastgeber</p>
        </div>
        </div>

      </div>
      </Collapsible>
         

        </div>

    );
}