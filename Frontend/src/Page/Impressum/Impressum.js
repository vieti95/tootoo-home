import React from "react";
import "./Impressum.css";
import Profilbild from "../Picture/1626354838592.jpg"
import { FaLanguage } from "react-icons/fa";
import { TbWorld } from "react-icons/tb";


const Impressum = () => {
  return (
   <>
   <div>
    <h1 className='Titel-Unterkunkft'>Über Anna</h1>
    </div>
   <div className="Usermod">
   
    <img className="Bild" src={Profilbild} alt="Gastgeberin" width="300" height="300"></img>

    <div className="Sprachenmodul">
    <FaLanguage className="Sprache"></FaLanguage>
    <p className="Sprache_Text">Spricht Chinesisch, Englisch, and Deutsch</p>
    </div>

    <div className="Lebenmodul">
    <TbWorld className="Leben"></TbWorld>
    <p className="Leben_Text">Lebt in Deutschland</p>
    </div>
    
   </div>
   </>
  );
};

export default Impressum;
