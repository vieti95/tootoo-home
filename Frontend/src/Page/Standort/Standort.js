import React from "react";
import "./Standort.css";
import { FaBus } from "react-icons/fa";
import LocalGroceryStoreIcon from '@mui/icons-material/LocalGroceryStore';
import { MdLocalAirport } from "react-icons/md";
import { FaTrain } from "react-icons/fa";
import EventIcon from '@mui/icons-material/Event';

const Standort = () => {
  return (
    <>
    <div className="Standort">
    <iframe className="maps" width="751" height="634" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" id="gmap_canvas" src="https://maps.google.com/maps?width=851&amp;height=634&amp;hl=en&amp;q=Wichern%20Stra%C3%9Fe%206%20Friedrichshafen+(TooToo-Home)&amp;t=k&amp;z=18&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe> 
    <div>
    <h2 className="Titel">
    Hier wirst du sein
    </h2>
    <h3 className="SubTitel">
    Friedrichshafen, Baden-Württemberg, Deutschland
    </h3>
    <p className="infotext">
    Eine kleine Strasse mit Reihenhäusern in einem kleinen Wohnviertel. 
    </p>
    <p className="infotext2">
    Ruhige Nachbarschaft, 100m von der Hauptstrasse.
    </p>

     <div className="Busmodul">
     <FaBus className="Bus"></FaBus>
     <p className="Bus_Text">100m zur Bushaltestelle (heisst Siedlung Löwental) </p>
     </div>

     <div className="Supermarktmodul">
     <LocalGroceryStoreIcon className="Supermarkt"></LocalGroceryStoreIcon>
     <p className="Supermarkt_Text">150m von diversen Supermärkten </p>
     </div>

     <div className="Flughafenmodul">
     <MdLocalAirport className="Flughafen"></MdLocalAirport>
     <p className="Flughafen_Text">5km vom Flughafen</p>
     </div>

     <div className="Bahnhofmodul">
     <FaTrain className="Bahnhof"></FaTrain>
     <p className="Bahnhof_Text">3km vom Bahnhof</p>
     </div>

     <div className="Messemodul">
     <EventIcon className="Messe"></EventIcon>
     <p className="Messe_Text">2km von der Messe entfernt.</p>
     </div>
    </div>
    </div>
    </>
  );
};

export default Standort;




















