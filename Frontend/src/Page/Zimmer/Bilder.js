import React from "react";
import './Bilder.css';
import {useRef } from "react";
import ScrollToTop from "./ScrollToTop";
import Homebuilder from "../Picture/IMG-20230324-WA0004.jpg";
import Draussen from "../Picture/IMG-20230324-WA0005.jpg";
import Wohnzimmer from "../Picture/IMG-20230324-WA0011.jpg";
import Zimmer101 from "../Picture/IMG-20230324-WA0017.jpg";
import Zimmer102 from "../Picture/IMG-20230324-WA0016.jpg";
import Zimmer103 from "../Picture/IMG-20230324-WA0018.jpg";
import Zimmer201 from "../Picture/IMG-20230324-WA0000.jpg";
import Zimmer202 from "../Picture/IMG-20230324-WA0001.jpg";
import Zimmer301 from "../Picture/IMG-20230324-WA0012.jpg";
import Zimmer302 from "../Picture/WhatsApp Image 2023-10-27 at 14.41.52.jpeg";
import Kitchen1 from "../Picture/IMG-20230324-WA0013.jpg";
import Kitchen2 from "../Picture/IMG-20230324-WA0014.jpg";
import Kitchen3 from "../Picture/IMG-20230324-WA0015.jpg";
import WC1 from "../Picture/IMG-20230324-WA0002.jpg";
import WC2 from "../Picture/IMG-20230324-WA0007.jpg";
import Bad1 from "../Picture/IMG-20230324-WA0009.jpg";
import Bad101 from "../Picture/WhatsApp Image 2023-10-27 at 14.41.12.jpeg"
import Bad2 from "../Picture/IMG-20230324-WA0010.jpg";
import Bad201 from "../Picture/IMG-20230325-WA0000.jpg";
import Waschmaschine from "../Picture/WhatsApp Image 2023-10-27 at 14.40.38.jpeg"


export default function Bilder()
{
    const Homebuilderref = useRef(null);
    const Wohnzimmerref = useRef(null);
    const Küchenref = useRef(null);
    const Schlafzimmer1ref = useRef(null);
    const Schlafzimmer2ref = useRef(null);
    const Schlafzimmer3ref = useRef(null);
    const Bad1ref = useRef(null);
    const Bad2ref = useRef(null);
    const WC1ref = useRef(null);
    const WC2ref = useRef(null);
    const Waschmaschineref = useRef(null);
    const scrollToSection = (elementRef) => {
      window.scrollTo({
        top: elementRef.current.offsetTop,
        behavior: "smooth",
      });
    };
      return (
        <div className="ZimmerBoard">
        <ScrollToTop />
        <div className="Bilder-Text">
        <h3>Fotorundang</h3>
        </div>
      <button className="Homebuilder-button"
        onClick={() => scrollToSection(Homebuilderref)}
      > <img className="Homebuilderklein" src={Homebuilder} alt="Homebuilder"></img>
       <p>Gemeinsam genutzter Außenbereich</p>
      </button>

      <button className="Wohnzimmer-button"
        onClick={() => scrollToSection(Wohnzimmerref)}
      > <img className="Wohnzimmerklein" src={Wohnzimmer} alt="Wohnzimmer"></img>
       <p className="Wohnzimmertext">Gemeinsam genutztes Wohnzimmer</p>
      </button>

      <button className="Küchen-button"
        onClick={() => scrollToSection(Küchenref)}
      > <img className="Küchenklein" src={Kitchen3} alt="Kitchen3"></img>
       <p>Gemeinsam genutzte ganze Küche</p>
      </button>

      <button className="Schlafzimmer1-button"
        onClick={() => scrollToSection(Schlafzimmer1ref)}
      > <img className="Schlafzimmer1klein" src={Zimmer101} alt="Zimmer101"></img>
       <p>Schlafzimmer 1</p>
      </button>

      <button className="Schlafzimmer2-button"
        onClick={() => scrollToSection(Schlafzimmer2ref)}
      > <img className="Schlafzimmer2klein" src={Zimmer201} alt="Zimmer201"></img>
       <p>Schlafzimmer 2</p>
      </button>

      <button className="Schlafzimmer3-button"
        onClick={() => scrollToSection(Schlafzimmer3ref)}
      > <img className="Schlafzimmer3klein" src={Zimmer301} alt="Zimmer301"></img>
       <p>Schlafzimmer 3</p>
      </button>

      <button className="Bad1-button"
        onClick={() => scrollToSection(Bad1ref)}
      > <img className="Bad1klein" src={Bad1} alt="Bad1"></img>
       <p>Gemeinsam genutztes Badezimmer 1</p>
      </button>

      <button className="Bad2-button"
        onClick={() => scrollToSection(Bad2ref)}
      > <img className="Bad2klein" src={Bad2} alt="Bad2"></img>
       <p>Gemeinsam genutztes Badezimmer 2</p>
      </button>

      <button className="WC1-button"
        onClick={() => scrollToSection(WC1ref)}
      > <img className="WC1klein" src={WC1} alt="WC1"></img>
       <p>Gemeinsam genutztes Gäste-WC 1</p>
      </button>

      <button className="WC2-button"
        onClick={() => scrollToSection(WC2ref)}
      > <img className="WC2klein" src={WC2} alt="WC2"></img>
       <p>Gemeinsam genutztes Gäste-WC 2</p>
      </button>

      <button className="Waschmaschine-button"
        onClick={() => scrollToSection(Waschmaschineref)}
      > <img className="Waschmaschineklein" src={Waschmaschine} alt="Waschmaschine"></img>
       <p>Gemeinsam genutzte Waschküche</p>
      </button>

      <div  ref={Homebuilderref}>
      <h2 className="Homebuilder-Text">Gemeinsam genutzter Außenbereich</h2>
      <img className="Homebuildergross" src={Homebuilder} alt="Homebuilder"></img>
      <img className="Draussen" src={Draussen} alt="Draussen"></img>
      </div>

      <div  ref={Wohnzimmerref}>
      <h2 className="Wohnzimmer-Text">Gemeinsam genutztes Wohnzimmer</h2>
      <img className="Wohnzimmergross" src={Wohnzimmer} alt="Wohnzimmer"></img>
      </div>

      <div  ref={Küchenref}>
      <h2 className="Küchen-Text">Gemeinsam genutztes Küche</h2>
      <img className="Küchengross" src={Kitchen3} alt="Kitchen3"></img>
      <img className="Kitchen2" src={Kitchen2} alt="Kitchen2"></img>
      <img className="Kitchen1" src={Kitchen1} alt="Kitchen1"></img>
      </div>

      <div  ref={Schlafzimmer1ref}>
      <h2 className="Schlafzimmer1-Text">Schlafzimmer 1</h2>
      <p className="Schlafzimmer1untertext">2 Einzelbetten</p>
      <img className="Schlafzimmer1gross" src={Zimmer101} alt="Zimmer101"></img>
      <img className="Zimmer102" src={Zimmer102} alt="Zimmer102"></img>
      <img className="Zimmer103" src={Zimmer103} alt="Zimmer103"></img>
      </div>

      <div  ref={Schlafzimmer2ref}>
      <h2 className="Schlafzimmer2-Text">Schlafzimmer 2</h2>
      <p className="Schlafzimmer2untertext">2 Einzelbetten</p>
      <img className="Schlafzimmer2gross" src={Zimmer201} alt="Zimmer201"></img>
      <img className="Zimmer202" src={Zimmer202} alt="Zimmer202"></img>
      </div>

      <div  ref={Schlafzimmer3ref}>
      <h2 className="Schlafzimmer3-Text">Schlafzimmer 3</h2>
      <p className="Schlafzimmer3untertext">3 Einzelbetten</p>
      <img className="Schlafzimmer3gross" src={Zimmer301} alt="Zimmer301"></img>
      <img className="Zimmer302" src={Zimmer302} alt="Zimmer302"></img>
      </div>
      
      <div  ref={Bad1ref}>
      <h2 className="Bad1-Text">Gemeinsam genutztes Badezimmer 1</h2>
      <img className="Bad1gross" src={Bad1} alt="Bad1"></img>
      <img className="Bad101" src={Bad101} alt="Bad101"></img>
      </div>

      <div  ref={Bad2ref}>
      <h2 className="Bad2-Text">Gemeinsam genutztes Badezimmer 2</h2>
      <img className="Bad2gross" src={Bad2} alt="Bad2"></img>
      <img className="Bad201" src={Bad201} alt="Bad201"></img>
      </div>

      <div  ref={WC1ref}>
      <h2 className="WC1-Text">Gemeinsam genutztes Gäste-WC 1</h2>
      <img className="WC1gross" src={WC1} alt="WC1"></img>
      </div>

      <div  ref={WC2ref}>
      <h2 className="WC2-Text">Gemeinsam genutztes Gäste-WC 2</h2>
      <img className="WC2gross" src={WC2} alt="WC2"></img>
      </div>

      <div  ref={Waschmaschineref}>
      <h2 className="Waschmaschine-Text">Gemeinsam genutzte Waschküche</h2>
      <img className="Waschmaschinegross" src={Waschmaschine} alt="Waschmaschine"></img>
      </div>
      </div>
      );
    
}