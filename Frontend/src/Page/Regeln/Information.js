import React from "react";
import "./Information.css";
import Icon from '@mdi/react';
import Collapsible from './Collapsible.js';
import { IoTimeOutline } from "react-icons/io5";
import { FaPeopleRoof } from "react-icons/fa6";
import { MdOutlinePets } from "react-icons/md";
import { MdOutlineSmokeFree } from "react-icons/md";
import { mdiSmokeDetectorOffOutline } from '@mdi/js';
import { mdiSmokeDetectorVariant } from '@mdi/js';
import { FaHome } from "react-icons/fa";
import BedroomChildIcon from '@mui/icons-material/BedroomChild';
import { AiFillSound } from "react-icons/ai";
import { GiStairs } from "react-icons/gi";


const Information = () => {
  return (
    <>
    <div>
    <h1 className='Titel-Unterkunkft'>Was du wissen solltest</h1>
    </div>
    <Collapsible summary="Hausregeln">
    <div className='Checkmodul'> 
    <h2 className="Check_Titel">Check-in und Check-out</h2>
        <IoTimeOutline className='Checkin'></IoTimeOutline>
         <p className='Checkin_Text'>Check-in nach 17:00 Uhr</p>
    <IoTimeOutline className='Checkout'></IoTimeOutline>
        <p className='Checkout_Text'>Check-out vor 12:00 Uhr</p>
    </div>
    <div className="Aufenhaltmodul">
    <h2 className="Aufenhalt_Titel">Während deines Aufenthaltes</h2>
        <FaPeopleRoof className='Mehrmenschen'></FaPeopleRoof>
        <p className='Mehrmenschen_Text'>Höchstens 12 Gäste</p>
        <MdOutlinePets className="Tiere"></MdOutlinePets>
        <p className='Tiere_Text'>Keine Haustiere</p>
    <MdOutlineSmokeFree className="Rauchen"></MdOutlineSmokeFree>
    <p className="Rauchen_Text">Rauchen verboten</p>
    </div>
    </Collapsible>

    <Collapsible summary="Sicherheit und die Unterkunft">
    <p className="Sicherheit_Unterkunft_Text">Vermeide Überraschungen, indem du diese wichtigen Details zur Unterkunft deines Gastgebers oder deiner Gastgeberin prüfst.</p>
    <div className='Sicherheitmodul'> 
    <h2 className="Sicherheit_Titel">Sicherheitseinrichtungen</h2>
         <Icon className="kohlenoxid" path={mdiSmokeDetectorOffOutline} size={2.5} />
         <p className='kohlenoxid_Text'>Kein Kohlenmonoxidmelder angegeben</p>
         <p className="kohlenoxid_Text2">Die Gastgeberin oder der Gastgeber hat keinen Kohlenmonoxidmelder in der Unterkunft angegeben. Wir empfehlen, einen tragbaren Melder für deine Reise mitzubringen.</p>
         <Icon className='Rauchmelder' path={mdiSmokeDetectorVariant} size={2.5} />
         <p className='Rauchmelder_Text'>Rauchmelder installiert</p>
    </div>
    <div className="Informationunterkunftmodul">
    <h2 className="Informationunterkunft_Titel">Informationen zur Unterkunft</h2>
        <FaHome className='UnterkunftHome'></FaHome>
        <p className='UnterkunftHome_Text'>5 Schlafzimmer, 12 Einzelbetten, Gemeinsam genutztes Bad</p>
        <p className="UnterkunftHome_Text2">Geeignet für Monteure und Arbeiter.</p>
        <BedroomChildIcon className="Teilenzimmer"></BedroomChildIcon>
        <p className='Teilenzimmer_Text'>Einige Räume werden gemeinsam genutzt</p>
        <AiFillSound className="Lautstaerke"></AiFillSound>
        <p className="Lautstaerke_Text">Lärm-Potenzial</p>
        <GiStairs className="Treppe"></GiStairs>
        <p className="Treppe_Text">Treppensteigen ist erforderlich</p>
    </div>
    </Collapsible>

    <Collapsible summary="Stonierungsbedingung">
    <p className="Stonierung">Bitte vor 48h stonierung. Nach 48h wird den Betrag nicht zurück erstatten</p>
    </Collapsible>
    
    </>
  );
};

export default Information;
