import Homebuilder from "../Picture/IMG-20230324-WA0004.jpg";
import Draussen from "../Picture/IMG-20230324-WA0005.jpg";
import Wohnzimmer from "../Picture/IMG-20230324-WA0011.jpg";
import Zimmer101 from "../Picture/IMG-20230324-WA0017.jpg";
import Zimmer102 from "../Picture/IMG-20230324-WA0016.jpg";
import Zimmer103 from "../Picture/IMG-20230324-WA0018.jpg";
import Zimmer201 from "../Picture/IMG-20230324-WA0000.jpg";
import Zimmer202 from "../Picture/IMG-20230324-WA0001.jpg";
import Zimmer301 from "../Picture/IMG-20230324-WA0012.jpg";
import Zimmer302 from "../Picture/WhatsApp Image 2023-10-27 at 14.41.52.jpeg";
import Kitchen1 from "../Picture/IMG-20230324-WA0013.jpg";
import Kitchen2 from "../Picture/IMG-20230324-WA0014.jpg";
import Kitchen3 from "../Picture/IMG-20230324-WA0015.jpg";
import WC1 from "../Picture/IMG-20230324-WA0002.jpg";
import WC2 from "../Picture/IMG-20230324-WA0007.jpg";
import Bad1 from "../Picture/IMG-20230324-WA0009.jpg";
import Bad2 from "../Picture/IMG-20230324-WA0010.jpg";
import Bad201 from "../Picture/IMG-20230325-WA0000.jpg";


export const HomepageData=[
{
    image: Homebuilder,
    heading: "Slide One"
},
{
    image: Draussen,
    heading: "Slide Two"
},
{
    image: Wohnzimmer,
    heading: "Slide Three"
},
{
    image: Zimmer101,
    heading: "Slide Fourth"
},
{
    image: Zimmer102,
    heading: "Slide Fifth"
},
{
    image: Zimmer103,
    heading: "Slide Sixth"
},
{
    image: Zimmer201,
    heading: "Slide Seventh"
},
{
    image: Zimmer202,
    heading: "Slide eigenth"
},
{
    image: Zimmer301,
    heading: "Slide nineth"
},
{
    image: Zimmer302,
    heading: "Slide tenth"
},
{
    image: Kitchen1,
    heading: "Slide eleventh"
},
{
    image: Kitchen2,
    heading: "Slide twelveth"
},
{
    image: Kitchen3,
    heading: "Slide thirdteenth"
},
{
    image: WC1,
    heading: "Slide Fourteenth"
},
{
    image: WC2,
    heading: "Slide Fifthteenth"
},
{
    image: Bad1,
    heading: "Slide Sixtheenth"
},
{
    image: Bad2,
    heading: "Slide Seventheenth"
},
{
    image: Bad201,
    heading: "Slide Eighttheenth"
}
];
