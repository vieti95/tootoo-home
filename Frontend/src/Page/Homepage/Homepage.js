import './Homepage.css';
import React from 'react';
import { useState, useEffect, useRef} from 'react';
import { AiOutlineArrowLeft, AiOutlineArrowRight} from "react-icons/ai";
import { HomepageData } from './HomepageData';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { BsFillPeopleFill } from "react-icons/bs";
import { AiOutlinePlusCircle } from "react-icons/ai";
import { AiOutlineMinusCircle } from "react-icons/ai";
import { useNavigate } from 'react-router-dom';


export default function Homepage()
{
  const [currentSlide, setCurrentSlide] =useState(0)
  const slideLength =HomepageData.length;
  const timeoutRef= useRef(null);
  const delay=2500;
  const navi =useNavigate();
  const [date, setDate] = useState(new Date());
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [open, setOpen]=useState();
  const [Erwachcounter, ErwachsetCounter] = useState(0);
  const [counter, setCounter] = useState(0);
  const [Kindcounter, KindsetCounter] = useState(0);
 
  const handleSubmit = (startDate, endDate, Erwachcounter, Kindcounter) => {
    console.log(startDate);
    console.log(endDate);
    console.log(Erwachcounter);
    console.log(Kindcounter);
    navi('/formular', {
      state: {
        anreise: startDate,
       abreise: endDate,
        erwachsene: Erwachcounter,
        kinder: Kindcounter,
      },
    });
  };
  const Erwachincrease = () => {
    ErwachsetCounter(count => count + 1);
  };
 
  const Erwachdecrease = () => {
    if (Erwachcounter > 0) {
      ErwachsetCounter(count => count - 1);
    }
  };

  const Kindincrease = () => {
    KindsetCounter(count => count + 1);
  };
 
  const Kinddecrease = () => {
    if (Kindcounter > 0) {
      KindsetCounter(count => count - 1);
    }
  };

  const handleClickOpen = () => {
    setOpen(true);
    
  };

  const handleClose = () => {
    setOpen(false);
  };
  
  function resetTimeout() {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
  }

  const nextSlide =()=>{
    setCurrentSlide(currentSlide=== slideLength -1 ? 0 : currentSlide+1);
  };

  const prevSlide =()=>{
    setCurrentSlide(currentSlide=== 0 ? slideLength-1 : currentSlide-1);
  };

  useEffect(()=>{
    setCurrentSlide(0)
  },[])

  useEffect(()=>{
    timeoutRef.current=setTimeout(
      ()=>
      setCurrentSlide((prevIndex) =>
      prevIndex === HomepageData.length - 1 ? 0 : prevIndex + 1),
      delay
    );
    return () => {
      resetTimeout();
    };
  },[currentSlide])

    return(
        <div>
         
            <h1 className='Titel-Homepage'>TooToo-Home</h1>

            <div className='border'>
            <label className='Anreise'>Anreise</label> 
            <div>
          <DatePicker className='Startzeit'
           showTimeSelect
           minTime={new Date(0, 0, 0, 12, 30)}
           maxTime={new Date(0, 0, 0, 19, 0)}
           selected={startDate}
           onChange={date => setStartDate(date)}
           selectsStart
           startDate={startDate}
           dateFormat="dd.MM.yyyy h:mmaa"
          />
          </div>
         
          <div>
          <label className='Abreise'>Abreise</label> 
          <DatePicker className='Endzeit'
           selectsEnd
           selected={endDate}
           onChange={date => setEndDate(date)}
           endDate={endDate}
           showTimeSelect
           minTime={new Date(0, 0, 0, 10,30 )}
           maxTime={new Date(0, 0, 0, 15, 0)}
           dateFormat="dd.MM.yyyy h:mmaa"
           minDate={startDate}
           startDate={startDate}
           />
          </div>

          <div>
          <label className='Guest'>Personen</label> 
          <button className="Guest_Button" onClick={handleClickOpen}>
           <BsFillPeopleFill></BsFillPeopleFill>
           Gäste {Erwachcounter +Kindcounter}
           </button>
           <Dialog open={open} onClose={handleClose}>
           <DialogTitle>Wählen Sie die Anzahl der Person aus</DialogTitle>
           <DialogContent>
            <DialogContentText className='ErText'>
            Erwachsen 
            <AiOutlineMinusCircle className="Erminus" onClick={Erwachdecrease}></AiOutlineMinusCircle>
            <span className='Text'>{Erwachcounter}</span>
            <AiOutlinePlusCircle className="Erplus" onClick={Erwachincrease}></AiOutlinePlusCircle>
          </DialogContentText>
          <DialogContentText className='KiText'>
            Kinder
            <AiOutlineMinusCircle className="Kiminus" onClick={Kinddecrease}></AiOutlineMinusCircle>
            <span className='Text2'>{Kindcounter}</span>
            <AiOutlinePlusCircle className='Kiplus' onClick={Kindincrease}></AiOutlinePlusCircle>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <button className="GSubmit" onClick={handleClose}>Submit</button>
        </DialogActions>
      </Dialog>
      </div>
      <button className='Submit' onClick={()=>handleSubmit(startDate, endDate, Erwachcounter, Kindcounter)}>Submit</button>
      </div>
           
           <div className='Slider'>
            <AiOutlineArrowLeft className='arrow prev'onClick={prevSlide}/>
            <AiOutlineArrowRight className='arrow next'onClick={nextSlide}/>

            {HomepageData.map((slide, index)=>{
              return(
                <div className={index===currentSlide ? "slide current" : "slide"} key={index}>
                  {index===currentSlide && (
                  <>
                  <div className='Bild'>
                  <img src={slide.image} alt="slide"/>
                  </div>
                    <div className='content'>
                      <h2>{slide.heading}</h2>
                      <hr/>
                    </div>
                    </>
                  )}
                </div>
              )
            })}
           </div>
        </div>

    );
}