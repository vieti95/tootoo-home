import './App.css';
import { BrowserRouter as Router, Routes, Route} from "react-router-dom";
import Homepage from  "./Page/Homepage/Homepage";
import Menue from './Page/Navbar/Menue';
import Bilder from './Page/Zimmer/Bilder';
import Haus from './Page/Haus/FinalUnterunft';
import Standort from './Page/Standort/Standort';
import Information from './Page/Regeln/Information';
import Footer from './Page/Footer/Footer';
import Impressum from './Page/Impressum/Impressum';
import Formular from './Page/Formular/Formular';

function App() {

 
  return (
    <div clas>
    <Router>
      <Menue></Menue>
    <Routes>
      <Route path="/" element={<Homepage />} />
      <Route path="/bilder" element={<Bilder />} />
      <Route path="/unterkunft" element={<Haus />} />
      <Route path="/standort" element={<Standort />} />
      <Route path="/information" element={<Information />} />
      <Route path="/impressum" element={<Impressum />} />
      <Route path="/formular" element={<Formular />} />
    </Routes>
  </Router>
  <Footer></Footer>
  </div>
    
  );
}

export default App;
