version: '3'
services:
  www:
    #image: php:apache
    build: .
    volumes:
      - "/run/desktop/mnt/host/c/Users/Viet/Desktop/Privat/Projekt/tootoo-home/Backend/docker-php/src:/var/www/html"
    ports:
      - 9000:80
     
  db:
    image: mysql:latest
    #command: --defaulf-authentication-plugin=mysql_native_password
    restart: always
    environment:
      - MYSQL_DATABASE=tootoohome
      - MYSQL_USER=php_docker
      - MYSQL_PASSWORD=password
      - MYSQL_ALLOW_EMPTY_PASSWORD=1
   
  phpmyadmin:
    image: phpmyadmin:latest
    restart: always
    ports:
      - 9001:80
    environment:
      PMA_ARBITRARY: 1
