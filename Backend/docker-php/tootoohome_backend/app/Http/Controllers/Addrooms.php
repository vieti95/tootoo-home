<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rooms;
class Addrooms extends Controller
{
    /*public function show() {
        $Room = Rooms::create([
            'gender' => 'men',
            'firstName' => 'Khanh Viet',
            'lastName' => 'Duong',
            'birthday' => '1997.06.09',
            'birthplace'=>'Wiesbaden',
            'address' => 'Am Waldblick 7a',
            'zipcode' => '65479',
            'city' => 'Raunheim',
            'country' => 'Deutschland',
            'anfahrt' => 'PKW',
            'email' => 'test',
            'handynummer' => '015156261076',
            'arrivalDate' => '2024.04.09 06:00',
            'departureDate' => '2024.04.12 18:00',
            'adult' => 1,
            'child' => 2,
        ]);
    }*/

    public function insert(Request $request){
        
       $room = new Rooms;
       $room ->gender=$request->gender;
       $room ->firstName=$request->firstName;
       $room ->lastName=$request->lastName;
       $room ->birthday=$request->birthday;
       $room ->birthplace=$request->birthplace;
       $room ->address=$request->address;
       $room ->zipcode=$request->zipCode;
       $room ->country=$request->country;
       $room ->city=$request->city;
       $room ->email=$request->email;
       $room ->handynummer=$request->handynummer;
       $room ->anfahrt=$request->anfahrt;
       $room ->arrivalDate=date('Y-m-d H:i:s', (int) $request->arrivalDate);
       $room ->departureDate=date('Y-m-d H:i:s', (int) $request->departureDate);
       $room ->adult=$request->adult;
       $room ->child=$request->child;
       $room->save();
}

}
