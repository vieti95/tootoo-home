<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Rooms extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'gender',
        'firstName',
        'lastName',
        'birthday',
        'birthplace',
        'address',
        'zipcode',
        'city',
        'country',
        'anfahrt',
        'email',
        'handynummer',
        'arrivalDate',
        'departureDate',
        'adult',
        'child',
        
    ];
    

    protected $table ='rooms';

    protected $primaryKey = 'id';
}
