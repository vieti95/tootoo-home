<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Addrooms;
use App\Http\Middleware\Cors;

Route::get('/hallo', function () {
    echo "hallo";
});


Route::post(
    '/add',
    [Addrooms::class, 'insert']
)->name('add')->middleware([Cors::class]);;


/*Route::get(
    '/add',
    [Addrooms::class, 'show']
)->name('Addrooms');*/
